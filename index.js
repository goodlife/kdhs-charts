let age={
    "key": "Age",
    "values": [
    {
        "key": "15-19",
        "values": [
            {
                "key": "<5",
                "value": {
                    "value": 2.9
                }
            },
            {
                "key": "5-9",
                "value": {
                    "value": 45.6
                }
            },
            {
                "key": "10-14",
                "value": {
                    "value": 39.1
                }
            },
            {
                "key": "15>",
                "value": {
                    "value": 11.7
                }
            },
            {
                "key": "Don't know",
                "value": {
                    "value": 0.6
                }
            }
        ]
    },
    {
        "key": "20-24",
        "values": [
            {
                "key": "<5",
                "value": {
                    "value": 5.3
                }
            },
            {
                "key": "5-9",
                "value": {
                    "value": 37.8
                }
            },
            {
                "key": "10-14",
                "value": {
                    "value": 40.4
                }
            },
            {
                "key": "15>",
                "value": {
                    "value": 14.8
                }
            },
            {
                "key": "Don't know",
                "value": {
                    "value": 1.7
                }
            }
        ]
    },
    {
        "key": "25-29",
        "values": [
            {
                "key": "<5",
                "value": {
                    "value": 1.5
                }
            },
            {
                "key": "5-9",
                "value": {
                    "value": 37
                }
            },
            {
                "key": "10-14",
                "value": {
                    "value": 43.7
                }
            },
            {
                "key": "15>",
                "value": {
                    "value": 16.4
                }
            },
            {
                "key": "Don't know",
                "value": {
                    "value": 1.4
                }
            }
        ]
    },
    {
        "key": "30-34",
        "values": [
            {
                "key": "<5",
                "value": {
                    "value": 1.1
                }
            },
            {
                "key": "5-9",
                "value": {
                    "value": 31.5
                }
            },
            {
                "key": "10-14",
                "value": {
                    "value": 45.3
                }
            },
            {
                "key": "15>",
                "value": {
                    "value": 19.6
                }
            },
            {
                "key": "Don't know",
                "value": {
                    "value": 2.5
                }
            }
        ]
    },
    {
        "key": "35-39",
        "values": [
            {
                "key": "<5",
                "value": {
                    "value": 2
                }
            },
            {
                "key": "5-9",
                "value": {
                    "value": 24.1
                }
            },
            {
                "key": "10-14",
                "value": {
                    "value": 47.3
                }
            },
            {
                "key": "15>",
                "value": {
                    "value": 24.6
                }
            },
            {
                "key": "Don't know",
                "value": {
                    "value": 2
                }
            }
        ]
    },
    {
        "key": "40-44",
        "values": [
            {
                "key": "<5",
                "value": {
                    "value": 2.2
                }
            },
            {
                "key": "5-9",
                "value": {
                    "value": 17.8
                }
            },
            {
                "key": "10-14",
                "value": {
                    "value": 50.7
                }
            },
            {
                "key": "15>",
                "value": {
                    "value": 28.1
                }
            },
            {
                "key": "Don't know",
                "value": {
                    "value": 1.2
                }
            }
        ]
    },
    {
        "key": "45-49",
        "values": [
            {
                "key": "<5",
                "value": {
                    "value": 2.9
                }
            },
            {
                "key": "5-9",
                "value": {
                    "value": 18
                }
            },
            {
                "key": "10-14",
                "value": {
                    "value": 44.5
                }
            },
            {
                "key": "15>",
                "value": {
                    "value": 33.1
                }
            },
            {
                "key": "Don't know",
                "value": {
                    "value": 1.5
                }
            }
        ]
    }
]
}
// //todo get the urban and rlural first loop
// function createSingleGroupBar(keyGroup){
//     // first get the y values
//     let newDatain = keyGroup.values.map(nameObject =>  { return{ [nameObject.key]: nameObject.value}})
//     let joinedArray=newDatain.reduce(function(result, current) { return Object.assign(result, current);}, {})
//     //on joined object append key
//     joinedArray.key=keyGroup.key
//     return joinedArray
// }
//
// function returnfullBarGroup(allGroups){
//     let Groups=[]
//     let joinedGroups=allGroups.values.map(nameObject =>  { return createSingleGroupBar(nameObject)})
//     return joinedGroups
// }